<?php
/**
 * User  : Nikita
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Contain handlers for hooks views
 */

/**
 * Implements hook_views_api().
 */
function akuma_views_views_api() {
  return array(
    'api'  => 3.0,
    'path' => drupal_get_path('module', 'akuma_views') . '/views',
  );
}

/**
 * Implements hook_views_default_views().
 */
function akuma_views_views_default_views() {
  $path = './' . drupal_get_path('module', 'akuma_views') . '/views/default_views/*.inc';
  $views = array();
  foreach (glob($path) as $views_filename) {
    require_once($views_filename);
  }
  return $views;
}

function akuma_views_views_data() {
  $data = array();
  $data['node'] = array(
    'author_select' => array(
      'group'      => t('Content'),
      'title'      => t('Author UID (select list)'),
      'help'       => t('Filter by author, choosing from dropdown list.'),
      'filter'     => array(
        'handler' => 'akuma_views_handler_filter_author_select'
      ),
      'argument'   => array(
        'handler' => 'views_handler_argument_numeric',
      ),
      'field'      => array(
        'handler' => 'views_handler_field_user',
      ),
      'real field' => 'uid',
    )
  );
  return $data;
}

function akuma_views_views_data_alter(&$data) {

}
